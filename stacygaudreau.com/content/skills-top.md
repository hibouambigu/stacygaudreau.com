---
render: false
---

My favourite languages to work with include **Python**, **C++**, and **JavaScript**. There are others, but it's easier to say I'm a **polyglot** who loves to use whatever tools are right for the work at hand!

With a history of first-hand business experience, I'm especially interested in the **finance**, **business logic** and **KPI/infrastructure** monitoring sectors.

I'm available for work that leverages any of the following skills, as long as there are interesting problems to solve --
