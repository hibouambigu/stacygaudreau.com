---
title: 'Ray Painter'
shortTitle: 'Ray Painter'
description: 'A 3D raytraced rendering library with accompanying editor GUI, written in C++.'
stack: ['C++', 'CMake', 'JUCE', 'Figma']
codeUrl: ''
demoUrl: 'https://stacygaudreau.com/projects/raypainter'
isWorkInProgress: true
disableDemoBtn: true
projectType: 'C++ Library and Cross-platform GUI Application'
type: 'project'
date: 2022-03-02T10:06:03-05:00
draft: true
---

## Ray-tracing 3D Library: `raytracerlibs`

Doot deet doot I'm a description
