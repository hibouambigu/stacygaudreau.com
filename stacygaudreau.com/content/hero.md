---
render: false
headline: "Hey, I'm Stacy. <br> I turn <em>ideas</em> into code."
btnText: 'See My Projects'
---

I'm a **software developer**.

In a past life, I founded an audio synthesizer company and developed electronic hardware.

Now I work with **software**, **desktops**, **servers** and the **web**.
